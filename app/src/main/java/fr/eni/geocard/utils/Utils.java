package fr.eni.geocard.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.common.util.ArrayUtils;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    private static final int PERMISSIONS_REQUEST_STATE = 1;

    public enum Unit {
        UNIT_K,
        UNIT_N
    }

    public static double calculateDistance(double lat1, double lon1, double lat2, double lon2, Unit unit) {
        double radlat1 = Math.PI * lat1 / 180;
        double radlat2 = Math.PI * lat2 / 180;
        double theta = lon1 - lon2;
        double radtheta = Math.PI * theta / 180;
        double dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit == Unit.UNIT_K) {
            dist = dist * 1.609344;
        }
        if (unit == Unit.UNIT_N) {
            dist = dist * 0.8684;
        }
        return dist;
    }

    public static void requestPermissions(Activity activity, int requestCode, String... permissions){
        List<String> toCheck = new ArrayList<>();

        if (permissions.length == 0){
            List<String> allPerm = new ArrayList<>();
            try {
                PackageInfo info = activity.getPackageManager().getPackageInfo(activity.getPackageName(), PackageManager.GET_PERMISSIONS);
                if (info.requestedPermissions != null) {
                    for (String p : info.requestedPermissions) {
                        allPerm.add(p);
                    }
                }
                permissions = allPerm.toArray(new String[0]);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }

        // check already granted permission
        for(int i = 0; i < permissions.length; i++){
            if (ContextCompat.checkSelfPermission(activity, permissions[i]) != PackageManager.PERMISSION_GRANTED){
                toCheck.add(permissions[i]);
            }
        }

        if (toCheck.size() > 0){
            // request the permission
            ActivityCompat.requestPermissions(activity,
                    toCheck.toArray(new String[0]),
                    requestCode);
        }
    }

}
