package fr.eni.geocard;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.eni.geocard.bo.Media;
import fr.eni.geocard.bo.PostCard;
import fr.eni.geocard.fragments.PlayerFragment;
import fr.eni.geocard.fragments.VisuPictureFragment;
import fr.eni.geocard.fragments.VisuTextFragment;

public class VisualisationActivity extends AppCompatActivity implements VisuPictureFragment.OnFragmentVisuPictureListener, VisuTextFragment.OnFragmentVisuTextListener {

    private PostCard postCard;
    private Media picture;
    private Media music;
    private Media video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualisation);

        Intent intent = getIntent();
        postCard = intent.getParcelableExtra(MainActivity.EXTRA_SLCT_POSTCARD);
        for (Media media : postCard.getMedias()) {
            switch (media.getType()) {
                case "PICTURE":
                    this.picture = media;
                    break;
                case "MUSIC":
                    this.music = media;
                    break;
                case "VIDEO":
                    this.video = media;
                    break;
            }
        }

        ImageView imageView = findViewById(R.id.frag_visu_img);
        imageView.setImageURI(Uri.parse(this.picture.getUrl()));

        TextView txtTitle = findViewById(R.id.visu_text_title);
        txtTitle.setText(this.postCard.getTitle());

        TextView txtMessage = findViewById(R.id.visu_text_message);
        if (this.postCard.getMessage() == null) {
            findViewById(R.id.visu_text_message).setVisibility(View.INVISIBLE);
        } else {
            txtMessage.setText(this.postCard.getMessage());
        }

        if (this.music == null) {
            findViewById(R.id.visu_card_view_title_music).setVisibility(View.INVISIBLE);
        } else {
            PlayerFragment playerFragment = (PlayerFragment) getSupportFragmentManager().findFragmentById(R.id.visu_music_player);
            if (playerFragment != null && playerFragment.isAdded()){
                playerFragment.setTitle(music.getDescription());
                playerFragment.setUrl(music.getUrl());
            }
        }

        TextView txtVideoLink = findViewById(R.id.visu_text_link_video);
        if (this.video == null) {
            findViewById(R.id.visu_card_view_title_video).setVisibility(View.INVISIBLE);
        } else {
            txtVideoLink.setText(this.video.getUrl());
        }
        visuLoaded();

    }

    @Override
    public void visuLoaded() {
        VisuTextFragment visuTextFragment = (VisuTextFragment) getSupportFragmentManager().findFragmentById(R.id.visu_frag_text);
        if (music != null && visuTextFragment != null && visuTextFragment.isAdded()){
            if (visuTextFragment.playerFragment != null){
                visuTextFragment.playerFragment.setUrl(music.getUrl());
                visuTextFragment.playerFragment.setTitle(music.getDescription());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
