package fr.eni.geocard;

import android.content.DialogInterface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Dictionary;

import fr.eni.geocard.exceptions.FormException;
import fr.eni.geocard.fragments.FormAddFragment;
import fr.eni.geocard.fragments.PhotoFragment;
import fr.eni.geocard.services.PostCardService;

public class addCardActivity extends AppCompatActivity implements PhotoFragment.OnFragmentPhotoListener, FormAddFragment.OnFragmentFormAddListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
    }

    @Override
    public void onSubmitFormAdd() {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_btn_save:
                saveAddForm();
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void saveAddForm() {
        PhotoFragment photoFragment = (PhotoFragment) getSupportFragmentManager().findFragmentById(R.id.frag_input_img);
        FormAddFragment formAddFragment = (FormAddFragment) getSupportFragmentManager().findFragmentById(R.id.frag_input_form);
        boolean hasError = false;
        if (photoFragment != null && photoFragment.isAdded() && formAddFragment != null && formAddFragment.isAdded()){
            try {
                String selectedPhoto = photoFragment.getSelectedPhoto();
                Dictionary<String, Object> formDatas = formAddFragment.getFormDatas();
                formDatas.put(PostCardService.VALUE_IMAGE, selectedPhoto);
                PostCardService.getInstance().create(formDatas, getApplication());
            } catch (FormException e) {
                alertError(e.getMessage());
                hasError = true;
            } catch(Exception e){
                e.printStackTrace();
                alertError("Image requise");
                hasError = true;
            }
        }
        else
            hasError = true;

        if (!hasError){
            Toast.makeText(this, R.string.card_create_ok, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void alertError(String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(addCardActivity.this);
        builder.setNegativeButton(R.string.alert_negative, (dialogInterface, i) -> {

        });
        builder.setTitle("Erreurs");
        builder.setMessage(msg);
        builder.create().show();
    }

}
