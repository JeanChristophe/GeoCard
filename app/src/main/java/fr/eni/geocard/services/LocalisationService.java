package fr.eni.geocard.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

/**
 * Background localisation service
 */
public class LocalisationService extends Service {

    private static HandlerThread handlerThread;
    public static final String REALTIME_POSITION = "fr.eni.geoservice.REALTIME_POSITION";
    public static final String EXTRA_LOCATION = "location";
    public static final String EXTRA_LOCATION_LATITUDE = "location_latitude";
    public static final String EXTRA_LOCATION_LONGITUDE = "location_longitude";
    private static final String TAG = "LocalisationService";
    public static Location lastLocation;

    private LocationManager mLocationManager;

    private final LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(final Location location) {
            if (location != null) {
                lastLocation = location;

                // Broadcast position
                Intent intentBroadcast = new Intent();
                intentBroadcast.setAction(REALTIME_POSITION);
                intentBroadcast.addCategory(Intent.CATEGORY_DEFAULT);
                intentBroadcast.putExtra(EXTRA_LOCATION, location);
                intentBroadcast.putExtra(EXTRA_LOCATION_LATITUDE, location.getLatitude());
                intentBroadcast.putExtra(EXTRA_LOCATION_LONGITUDE, location.getLongitude());
                sendBroadcast(intentBroadcast);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    public class LocalisationServiceBinder extends Binder {
        public LocalisationService getService() {
            return LocalisationService.this;
        }
    }

    protected final IBinder serviceBinder = new LocalisationServiceBinder();

    public LocalisationService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        launchService();
        return serviceBinder;
    }

    public void launchService() {
        if (handlerThread == null || !handlerThread.isAlive()) {
            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "launchService: Erreur de permission");
            } else {
                handlerThread = new HandlerThread(this.getClass().getSimpleName() + "Thread");
                handlerThread.start();
                Looper looper = handlerThread.getLooper();


                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000,
                        1f, mLocationListener, looper);

                lastLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                // Broadcast la dernière position connue
                mLocationListener.onLocationChanged(lastLocation);
            }
        }
    }

    public void requestLocation() {
        Location requested = null;
        if (mLocationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "requestLocation: Erreur de permission");
            }else {
                mLocationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, mLocationListener, null);
            }
        }
    }
}
