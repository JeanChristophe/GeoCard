package fr.eni.geocard.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

import java.io.IOException;

public class PlayerService extends Service {

    Thread playerThread;
    MediaPlayer player;

    public PlayerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void playUrl(String url){
        stop();
        playerThread = new Thread(() -> {
           player = new MediaPlayer();
            try {
                player.setDataSource(url);
                player.prepare();
                player.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        playerThread.start();
    }

    public void stop(){
        try {
            if (player != null && player.isPlaying()){
                player.stop();
            }
            if (playerThread != null && playerThread.isAlive()){
                playerThread.interrupt();
            }
        }catch (Exception e){
            if (playerThread != null && playerThread.isAlive()){
                playerThread.interrupt();
            }
        }
    }

    public boolean isPlaying(){
        boolean playing = false;
        try {
            playing = player != null && player.isPlaying();
        }catch (Exception e){
            playing = false;
        }
        return playing;
    }
}
