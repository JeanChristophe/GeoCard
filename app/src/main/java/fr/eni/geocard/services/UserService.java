package fr.eni.geocard.services;

import android.app.Application;
import android.arch.lifecycle.LifecycleOwner;

import java.util.List;

import fr.eni.geocard.Repository.UserRepository;
import fr.eni.geocard.bo.User;

public class UserService {
    private static UserService instance;

    private UserService() {
    }

    public static UserService getInstance() {
        if (instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    public void getById(int userId, Application application, LifecycleOwner lifecycleOwner, UserListener listener) {
        UserRepository userRepository = new UserRepository(application);

        userRepository.getById(userId).observe(lifecycleOwner, user -> {
            listener.onGetOneResponse(user);
        });
    }

    public interface UserListener {
        void onGetOneResponse(User user);
    }
}
