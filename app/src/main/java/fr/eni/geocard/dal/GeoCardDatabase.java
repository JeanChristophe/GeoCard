package fr.eni.geocard.dal;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import fr.eni.geocard.bo.Media;
import fr.eni.geocard.bo.PostCard;
import fr.eni.geocard.bo.User;

@Database(entities = {Media.class, PostCard.class, User.class}, version = 1)
public abstract class GeoCardDatabase extends RoomDatabase {

    public abstract UserDao userDao();
    public abstract PostCardDao postCardDao();
    public abstract MediaDao mediaDao();

    private static GeoCardDatabase instance;

    public static GeoCardDatabase getDatabase(final Context context) {
        if (instance == null) {
            synchronized (GeoCardDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            GeoCardDatabase.class, "GeoCardDatabase")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return instance;
    }
}
