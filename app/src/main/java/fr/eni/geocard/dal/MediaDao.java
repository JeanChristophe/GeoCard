package fr.eni.geocard.dal;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import fr.eni.geocard.bo.Media;

@Dao
public interface MediaDao {
    @Insert
    void insert(Media... medias);
    @Update
    void update(Media... medias);
    @Delete
    void delete(Media... medias);
    @Query("SELECT * FROM Medias WHERE card = :cardId AND type = :type")
    LiveData<Media> select(String cardId, String type);
    @Query("SELECT * FROM Medias WHERE card = :cardId")
    LiveData<List<Media>> selectByCard(String cardId);
    @Query("SELECT * FROM Medias")
    LiveData<List<Media>> selectAll();
}
