package fr.eni.geocard.dal;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import fr.eni.geocard.bo.User;

@Dao
public interface UserDao {
    @Insert
    void insert(User... users);
    @Update
    void update(User... users);
    @Delete
    void delete(User... users);
    @Query("SELECT * FROM Users WHERE id = :id")
    LiveData<User> select(int id);
    @Query("SELECT * FROM Users WHERE token_id = :tokenId")
    User selectByTokenId(String tokenId);
    @Query("SELECT * FROM Users ORDER BY username")
    LiveData<List<User>> selectAll();
    @Query("DELETE FROM Users")
    void deleteAll();
}
