package fr.eni.geocard.exceptions;

import java.util.ArrayList;
import java.util.List;

public class FormException extends Exception {
    private List<String> messages = new ArrayList<>();
    public FormException(){
        super();
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder();
        for(String m : messages){
            sb.append(m);
            sb.append(",\n");
        }
        return sb.toString();
    }

    public void addMessage(String message){
        messages.add(message);
    }

    public boolean hasMessage(){
        return messages.size() > 0;
    }
}
