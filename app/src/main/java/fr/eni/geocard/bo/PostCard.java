package fr.eni.geocard.bo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

@Entity
(
    tableName = "PostCards",
    foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "user")
)
public class PostCard implements Parcelable {
    @PrimaryKey
    @NonNull
    private String id;
    @NonNull
    private double latitude;
    @NonNull
    private double longitude;
    @NonNull
    private String title;
    private String message = "";
    @Ignore
    private User user;
    @NonNull
    @ColumnInfo(name = "user")
    private int userId;
    @Ignore
    private List<Media> medias;

    public PostCard() {
        medias = new ArrayList<>();
    }

    public PostCard(String id, double latitude, double longitude, String title, String message, User user) {
        this();
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.message = message;
        this.user = user;
        this.userId = user.getId();
    }

    protected PostCard(Parcel in) {
        id = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        title = in.readString();
        message = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
        userId = in.readInt();
        medias = in.createTypedArrayList(Media.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(title);
        dest.writeString(message);
        dest.writeParcelable(user, flags);
        dest.writeInt(userId);
        dest.writeTypedList(medias);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PostCard> CREATOR = new Creator<PostCard>() {
        @Override
        public PostCard createFromParcel(Parcel in) {
            return new PostCard(in);
        }

        @Override
        public PostCard[] newArray(int size) {
            return new PostCard[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    @NonNull
    public int getUserId() {
        return userId;
    }

    public void setUserId(@NonNull int userId) {
        this.userId = userId;
    }

    public List<Media> getMedias() {
        return medias;
    }

    public void setMedias(List<Media> medias) {
        this.medias = medias;
    }

    @Override
    public String toString() {
        return "PostCard{" +
                "id='" + id + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", user=" + user +
                ", userId=" + userId +
                '}';
    }
}
