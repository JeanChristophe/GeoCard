package fr.eni.geocard.bo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Relation;
import android.arch.persistence.room.TypeConverter;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

@Entity
(
    tableName = "Medias",
    foreignKeys = @ForeignKey(entity = PostCard.class,
        parentColumns = "id",
        childColumns = "card",
        onDelete = ForeignKey.CASCADE),
    primaryKeys = {"card", "type"}
)
public class Media implements Parcelable {

    public enum Type {
        TYPE_VIDEO("VIDEO"),
        TYPE_MUSIC("MUSIC"),
        TYPE_PICTURE("PICTURE"),
        TYPE_SKETCH("SKETCH");

        private String friendlyName;

        Type(String friendlyName){
            this.friendlyName = friendlyName;
        }

        @Override
        public String toString() {
            return friendlyName;
        }
    }

    @Ignore
    private PostCard card;
    @NonNull
    @ColumnInfo(name = "card")
    private String cardId;
    @NonNull
    private String type;
    @NonNull
    private String url;
    private String description = "";

    public Media() {
    }

    public Media(@NonNull String type, @NonNull String url, String description) {
        this.type = type;
        this.url = url;
        this.description = description;
    }

    public Media(PostCard card, String type, String url, String description) {
        this.card = card;
        this.type = type;
        this.url = url;
        this.description = description;
        this.cardId = card.getId();
    }

    protected Media(Parcel in) {
        card = in.readParcelable(PostCard.class.getClassLoader());
        cardId = in.readString();
        type = in.readString();
        url = in.readString();
        description = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(card, flags);
        dest.writeString(cardId);
        dest.writeString(type);
        dest.writeString(url);
        dest.writeString(description);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    public PostCard getCard() {
        return card;
    }

    public void setCard(PostCard card) {
        this.card = card;
        this.cardId = card.getId();
    }

    @NonNull
    public String getCardId() {
        return cardId;
    }

    public void setCardId(@NonNull String cardId) {
        this.cardId = cardId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.getDescription();
    }
}
